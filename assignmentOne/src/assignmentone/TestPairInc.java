/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package assignmentone;

import java.util.HashMap;

/**
 *
 * @author coed-forensic
 */
public class TestPairInc
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        TestPairInc p = new TestPairInc();
        HashMap< String, HashMap< String, Integer>> h1 = new HashMap< String, HashMap< String, Integer>>();
        TestPairInc.update1( h1, "hello", "world");
        System.out.println("" + h1.get( "hello").get( "world"));
        TestPairInc.update1( h1, "hello", "world");
        System.out.println("" + h1.get( "hello").get( "world"));
    }
    public static void update1( HashMap< String, HashMap< String, Integer>> map, String key1, String key2)
    {
        if( !map.containsKey( key1))
            map.put( key1, new HashMap< String, Integer>());
        if( !map.get( key1).containsKey( key2))
            map.get( key1).put( key2, 0);
        map.get( key1).put( key2, map.get( key1).get( key2) + 1);
    }
}
