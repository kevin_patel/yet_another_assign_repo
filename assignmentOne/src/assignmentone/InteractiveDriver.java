/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package assignmentone;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author coed-forensic
 */
public class InteractiveDriver 
{
    private static ArrayList<Integer> unify_index;

    private boolean data_generated = false;
    private boolean data_splitted = false;
    private boolean data_unk_fixed = false;
    private boolean freq_calculated = false;
    private boolean good_calculated = false;
    private boolean testing_done = false;
    private boolean model_generated = false;
    private String corpus_fname = "data/new_morph_brown.txt";
    //private String corpus_fname = "data/lower_brown.txt";
    //private String corpus_fname = "data/proc_brown.txt";
    private String model_prefix = "data/hmm_";
    private String data_prefix = "data/fullfold_";
    public int total_line_cnt = 0;
    public int total_word_cnt = 0;
    public int total_tag_cnt = 0;
    public static int fold_cnt = 1;
    public int[] fold_breaks = new int[]{ 0, 53440};
    
    private static Socket morpher_socket;
    private static PrintWriter sock_out;
    private static BufferedReader sock_in;
    
    //public int[] fold_breaks = new int[]{ 0, 10688, 21376, 32064, 42752, 53440};

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException 
    {
        // class initialization
        InteractiveDriver driver = new InteractiveDriver();
        //CrossValidDriver driver = new CrossValidDriver( false, false);
        HashSet< String> lvocab = new HashSet< String>();
        HashSet< String> ltagset = new HashSet< String>();    
        // <editor-fold desc="if data binaries do not exists, create them">
        if( !driver.isData_generated())
        {
            System.out.println("Creating individual folds");
            BufferedReader corpus_br = new BufferedReader( new FileReader( driver.corpus_fname));
            String cur_tagged_line = "";
            int line_ptr = 0;
            int cur_fold = 0;
            ArrayList< ArrayList< String>> cur_fold_data = new ArrayList< ArrayList< String>>();
            ArrayList< ArrayList< String>> cur_fold_labels = new ArrayList< ArrayList< String>>();

            FileOutputStream fos = new FileOutputStream("tp");
            ObjectOutputStream oos = new ObjectOutputStream( fos);
            
            while( true)
            {
                // read line if available
                cur_tagged_line = corpus_br.readLine();
                if( cur_tagged_line == null)
                    break;
                // check if new fold
                if( line_ptr == driver.fold_breaks[ cur_fold + 1])
                {
                    System.out.println( "Sizes = " + cur_fold_data.size() + " " + cur_fold_labels.size());
                    String raw_fname = driver.data_prefix;
                    raw_fname += cur_fold;
                    raw_fname += "_data.bin";
                    fos = new FileOutputStream( raw_fname);
                    oos = new ObjectOutputStream( fos);
                    oos.writeObject( cur_fold_data);
                    oos.close();
                    fos.close();
                    
                    String labels_fname = driver.data_prefix;
                    labels_fname += cur_fold;
                    labels_fname += "_labels.bin";
                    fos = new FileOutputStream( labels_fname);
                    oos = new ObjectOutputStream( fos);
                    oos.writeObject( cur_fold_labels);
                    oos.close();
                    fos.close();                            
                    cur_fold_data = new ArrayList< ArrayList< String>>();
                    cur_fold_labels = new ArrayList< ArrayList< String>>();
                    cur_fold ++;
                }
                // process line
                ArrayList< ArrayList< String>> pair_data = InteractiveDriver.parse_tagged_line(cur_tagged_line);
                driver.total_word_cnt += pair_data.get( 0).size();
                driver.total_tag_cnt += pair_data.get( 1).size();
                cur_fold_data.add( pair_data.get( 0));
                cur_fold_labels.add( pair_data.get( 1));
                
                // increment line counter
                line_ptr ++;
                // Done
            }
            driver.total_line_cnt = line_ptr;
            System.out.println( "Total lines = " + driver.total_line_cnt);
            System.out.println( "Total words = " + driver.total_word_cnt);
            System.out.println( "Total tags = " + driver.total_tag_cnt);
        }
        // </editor-fold>

        //<editor-fold desc="if data splits do not exists, create them">
        if( !driver.isData_splitted())
        {
            System.out.println("Creating train and test splits");
            //<editor-fold desc="load individual fold data and labels' lists">
            ArrayList< ArrayList< ArrayList< String>>> corpus_data = new ArrayList< ArrayList< ArrayList< String>>>();
            for( int fold_ptr = 0; fold_ptr < fold_cnt; fold_ptr ++)
            {
                FileInputStream fis = new FileInputStream( ( driver.data_prefix + fold_ptr + "_data.bin"));
                ObjectInputStream ois = new ObjectInputStream( fis);
                corpus_data.add( (ArrayList< ArrayList< String>>)ois.readObject());
                ois.close();
                fis.close();
            }
            ArrayList< ArrayList< ArrayList< String>>> corpus_labels = new ArrayList< ArrayList< ArrayList< String>>>();
            for( int fold_ptr = 0; fold_ptr < fold_cnt; fold_ptr ++)
            {
                FileInputStream fis = new FileInputStream( ( driver.data_prefix + fold_ptr + "_labels.bin"));
                ObjectInputStream ois = new ObjectInputStream( fis);
                corpus_labels.add( (ArrayList< ArrayList< String>>)ois.readObject());
                ois.close();
                fis.close();
            }
            //</editor-fold>
            
            // <editor-fold desc="split into train and test">
            for( int fold_ptr = 0; fold_ptr < fold_cnt; fold_ptr ++)
            {
                ArrayList< ArrayList< String>> train_data = new ArrayList< ArrayList< String>>();
                ArrayList< ArrayList< String>> test_data = new ArrayList< ArrayList< String>>();
                ArrayList< ArrayList< String>> train_labels = new ArrayList< ArrayList< String>>();
                ArrayList< ArrayList< String>> test_labels = new ArrayList< ArrayList< String>>();
                for( int rem_ptr = 0; rem_ptr < fold_cnt; rem_ptr ++)
                {
                    if( fold_ptr != rem_ptr)
                        continue;
                    train_data.addAll( corpus_data.get( rem_ptr));
                    train_labels.addAll( corpus_labels.get( rem_ptr));
                }
                //test_data.addAll( corpus_data.get( fold_ptr));
                //test_labels.addAll( corpus_labels.get( fold_ptr));
                
                // <editor-fold desc="write train and test">
                FileOutputStream fos;
                ObjectOutputStream oos;
                fos = new FileOutputStream( driver.data_prefix + fold_ptr + "_train_data");
                oos = new ObjectOutputStream( fos);
                oos.writeObject( train_data);
                oos.close();
                fos.close();
                fos = new FileOutputStream( driver.data_prefix + fold_ptr + "_train_labels");
                oos = new ObjectOutputStream( fos);
                oos.writeObject( train_labels);
                oos.close();
                fos.close();
//                fos = new FileOutputStream( driver.data_prefix + fold_ptr + "_test_data");
//                oos = new ObjectOutputStream( fos);
//                oos.writeObject( test_data);
//                oos.close();
//                fos.close();
//                fos = new FileOutputStream( driver.data_prefix + fold_ptr + "_test_labels");
//                oos = new ObjectOutputStream( fos);
//                oos.writeObject( test_labels);
//                oos.close();
//                fos.close();
                // </editor-fold>
                        
            }
            //</editor-fold>
        }
        //</editor-fold>

        //<editor-fold desc="if data not handlded for unk words, do it">
        if( !driver.isData_unk_fixed())
        {
            System.out.println("Fixing unknown words");
            for( int fold_ptr = 0; fold_ptr < fold_cnt; fold_ptr ++)
            {
                System.out.println( "Processing fold " + fold_ptr);
                //<editor-fold desc="reading them back">
                ArrayList< ArrayList< String>> train_data = new ArrayList< ArrayList< String>>();
                ArrayList< ArrayList< String>> train_labels = new ArrayList< ArrayList< String>>();
                ArrayList< ArrayList< String>> test_data = new ArrayList< ArrayList< String>>();
                ArrayList< ArrayList< String>> test_labels = new ArrayList< ArrayList< String>>();
                
                FileInputStream fis;
                ObjectInputStream ois;
                // reading train stuff
                fis = new FileInputStream( driver.data_prefix + fold_ptr + "_train_data");
                ois = new ObjectInputStream( fis);
                train_data = ( ArrayList< ArrayList< String>>) ois.readObject();
                ois.close();
                fis.close();
                fis = new FileInputStream( driver.data_prefix + fold_ptr + "_train_labels");
                ois = new ObjectInputStream( fis);
                train_labels = ( ArrayList< ArrayList< String>>) ois.readObject();
                ois.close();
                fis.close();
                // Done
                // reading test stuff
//                fis = new FileInputStream( driver.data_prefix + fold_ptr + "_test_data");
//                ois = new ObjectInputStream( fis);
//                test_data = ( ArrayList< ArrayList< String>>) ois.readObject();
//                ois.close();
//                fis.close();
//                fis = new FileInputStream( driver.data_prefix + fold_ptr + "_test_labels");
//                ois = new ObjectInputStream( fis);
//                test_labels = ( ArrayList< ArrayList< String>>) ois.readObject();
//                ois.close();
//                fis.close();
                // Done
                //</editor-fold>
                
                //<editor-fold desc="replacing with UNK wherever necessary">
                // creating local vocabs and tagsets
                
                for( int rptr = 0; rptr < train_data.size(); rptr ++)
                    for( int cptr = 0; cptr < train_data.get( rptr).size(); cptr ++)
                    {
                        String cur_word = train_data.get( rptr).get( cptr);
                        String cur_tag = train_labels.get( rptr).get( cptr);
                        lvocab.add( cur_word);
                        ltagset.add( cur_tag);
                    }
                // replacing if not in corresponding sets
                for( int rptr = 0; rptr < test_data.size(); rptr ++)
                {
                    for( int cptr = 0; cptr < test_data.get( rptr).size(); cptr ++)
                    {
                        String cur_word = test_data.get( rptr).get( cptr);
                        String cur_tag = test_labels.get( rptr).get( cptr);
                        if( !lvocab.contains( cur_word))
                            test_data.get( rptr).set( cptr, "_UNK_");
                        if( !ltagset.contains( cur_tag))
                            test_labels.get( rptr).set( cptr, "_UNK_");
                    }
                }
                //</editor-fold>
            
                //<editor-fold desc="writing them back">
                FileOutputStream fos;
                ObjectOutputStream oos;
                fos = new FileOutputStream( driver.data_prefix + fold_ptr + "_test_data_unk");
                oos = new ObjectOutputStream( fos);
                oos.writeObject( test_data);
                oos.close();
                fos.close();
                fos = new FileOutputStream( driver.data_prefix + fold_ptr + "_test_labels_unk");
                oos = new ObjectOutputStream( fos);
                oos.writeObject( test_labels);
                oos.close();
                fos.close();                
                fos = new FileOutputStream( driver.data_prefix + fold_ptr + "_vocab");
                oos = new ObjectOutputStream( fos);
                oos.writeObject( lvocab);
                oos.close();
                fos.close();
                fos = new FileOutputStream( driver.data_prefix + fold_ptr + "_tagset");
                oos = new ObjectOutputStream( fos);
                oos.writeObject( ltagset);
                oos.close();
                fos.close();                
                lvocab.add( "_UNK_");
                ltagset.add( "_UNK_");
                fos = new FileOutputStream( driver.data_prefix + fold_ptr + "_vocab_unk");
                oos = new ObjectOutputStream( fos);
                oos.writeObject( lvocab);
                oos.close();
                fos.close();
                fos = new FileOutputStream( driver.data_prefix + fold_ptr + "_tagset_unk");
                oos = new ObjectOutputStream( fos);
                oos.writeObject( ltagset);
                oos.close();
                fos.close();                
                
                //</editor-fold>
            }
        }
        //</editor-fold>
        
        //<editor-fold desc="Count various frequencies if necessary">
        if( !driver.isFreq_calculated())
        {
            System.out.println("Calculating frequencies");
            HashMap< String, Integer> just_tag = new HashMap< String, Integer>();
            HashMap< String, Integer> just_word = new HashMap< String, Integer>();
            HashMap< String, HashMap< String, Integer>> tag_given_tag = new HashMap< String, HashMap< String, Integer>>();
            HashMap< String, HashMap< String, Integer>> word_given_tag = new HashMap< String, HashMap< String, Integer>>();
            HashMap< String, HashMap< String, Integer>> tag_given_word = new HashMap< String, HashMap< String, Integer>>();

            for( int fold_ptr = 0; fold_ptr < fold_cnt; fold_ptr ++)
            {
                System.out.println("Processing fold " + fold_ptr);
                //<editor-fold desc="read train data">
                ArrayList< ArrayList< String>> train_data = new ArrayList< ArrayList< String>>();
                ArrayList< ArrayList< String>> train_labels = new ArrayList< ArrayList< String>>();
                FileInputStream fis;
                ObjectInputStream ois;
                fis = new FileInputStream( driver.data_prefix + fold_ptr + "_train_data");
                ois = new ObjectInputStream( fis);
                train_data = (ArrayList< ArrayList< String>>)ois.readObject();
                ois.close();
                fis.close();
                fis = new FileInputStream( driver.data_prefix + fold_ptr + "_train_labels");
                ois = new ObjectInputStream( fis);
                train_labels = (ArrayList< ArrayList< String>>)ois.readObject();
                ois.close();
                fis.close();
                //</editor-fold>

                //<editor-fold desc="process train data">
                for( int rptr = 0; rptr < train_data.size(); rptr ++)
                {
                    ArrayList< String> cur_data = train_data.get( rptr);
                    ArrayList< String> cur_labels = train_labels.get( rptr);
                    String prev_word = cur_data.get( 0);
                    String prev_tag = cur_labels.get( 0);

                    indi_inc( just_word, prev_word);
                    indi_inc( just_tag, prev_tag);
                    pair_inc( word_given_tag, prev_tag, prev_word);
                    pair_inc( tag_given_word, prev_word, prev_tag);

                    for( int cptr = 1; cptr < cur_data.size(); cptr ++)
                    {
                        String cur_word = cur_data.get( cptr);
                        String cur_tag = cur_labels.get( cptr);

                        indi_inc( just_word, cur_word);
                        indi_inc( just_tag, cur_tag);

                        pair_inc( tag_given_tag, prev_tag, cur_tag);
                        pair_inc( word_given_tag, cur_tag, cur_word);
                        pair_inc( tag_given_word, cur_word, cur_tag);

                        prev_word = cur_word;
                        prev_tag = cur_tag;
                    }
                }
                //</editor-fold>

                //<editor-fold desc="write everything to files :( .. I hate u JAVA">
                FileOutputStream fos;
                ObjectOutputStream oos;
                fos = new FileOutputStream( driver.data_prefix + fold_ptr + "_just_tags");
                oos = new ObjectOutputStream( fos);
                oos.writeObject( just_tag);
                oos.close();
                fos.close();
                fos = new FileOutputStream( driver.data_prefix + fold_ptr + "_just_word");
                oos = new ObjectOutputStream( fos);
                oos.writeObject( just_word);
                oos.close();
                fos.close();
                fos = new FileOutputStream( driver.data_prefix + fold_ptr + "_tag_given_tag");
                oos = new ObjectOutputStream( fos);
                oos.writeObject( tag_given_tag);
                oos.close();
                fos.close();
                fos = new FileOutputStream( driver.data_prefix + fold_ptr + "_tag_given_word");
                oos = new ObjectOutputStream( fos);
                oos.writeObject( tag_given_word);
                oos.close();
                fos.close();
                fos = new FileOutputStream( driver.data_prefix + fold_ptr + "_word_given_tag");
                oos = new ObjectOutputStream( fos);
                oos.writeObject( word_given_tag);
                oos.close();
                fos.close();
                //</editor-fold>
            }
        }
        //</editor-fold>
        
//        if( !driver.isGood_calculated())
//        {
//            System.out.println("Good Turing");
//            HashMap< String, Integer> just_tag = new HashMap< String, Integer>();
//            HashMap< String, Integer> just_word = new HashMap< String, Integer>();
//            HashMap< String, HashMap< String, Integer>> tag_given_tag = new HashMap< String, HashMap< String, Integer>>();
//            HashMap< String, HashMap< String, Integer>> word_given_tag = new HashMap< String, HashMap< String, Integer>>();
//            HashMap< String, HashMap< String, Integer>> tag_given_word = new HashMap< String, HashMap< String, Integer>>();
//            HashMap< Integer, Integer> emissions_with_freq = new HashMap< Integer, Integer>();
//            
//            for( int fold_ptr = 0; fold_ptr < fold_cnt; fold_ptr ++)
//            {
//                // only smoothing emissions
//                FileInputStream fis = new FileInputStream( driver.data_prefix + fold_ptr + "_word_given_tag");
//                ObjectInputStream ois = new ObjectInputStream( fis);
//                word_given_tag = ( HashMap< String, HashMap< String, Integer>>)ois.readObject();
//                ois.close();
//                fis.close();
//                
//                for( String tag: word_given_tag.keySet())
//                {
//                    for( String word: word_given_tag.get( tag).keySet())
//                    {
//                        Integer c = word_given_tag.get( tag).get( word);
//                        if( !emissions_with_freq.containsKey( c))
//                            emissions_with_freq.put( c, 0);
//                        Integer val = emissions_with_freq.get( c);
//                        emissions_with_freq.put( c, val + 1);
//                    }
//                }
//                FileOutputStream fos = new FileOutputStream( driver.data_prefix + fold_ptr + "_emissions_with_freq");
//                ObjectOutputStream oos = new ObjectOutputStream( fos);
//                oos.writeObject( emissions_with_freq);
//                oos.close();
//                fos.close();
//            }
//        }
        
        //<editor-fold desc="Testing">
        if( !driver.isTesting_done())
        {
            int fold_ptr = 0;
            System.out.println("Testing");
//            Baseline_Unigram tagger_1 = new Baeline_Unigram( fold_ptr);
//            Baseline_Bigram tagger_2 = new Baseline_Bigram( fold_ptr);
//            Bigram_BackOff_Unigram tagger_3 = new Bigram_BackOff_Unigram( fold_ptr);
//            Bigram_BackOff_Unigram_with_Interpolation tagger_4 = new Bigram_BackOff_Unigram_with_Interpolation(fold_ptr);
            Bigram_BackOff_Unigram_with_Interpolation_Add_One tagger_5 = new Bigram_BackOff_Unigram_with_Interpolation_Add_One( fold_ptr);
            //Bigram_BackOff_Unigram_with_Interpolation_Good_Turing tagger_6 = new Bigram_BackOff_Unigram_with_Interpolation_Good_Turing( fold_ptr);
            
            BufferedReader inp_br = new BufferedReader( new InputStreamReader( System.in));
            String inp_line = "";
            
            try
            {
                morpher_socket = new Socket( "127.0.0.1", 12345);
                sock_out = new PrintWriter( morpher_socket.getOutputStream(), true);
                sock_in = new BufferedReader( new InputStreamReader( morpher_socket.getInputStream()));
            }
            catch( Exception e)
            {
                System.out.println( "" + e.getMessage());
            }
            
            while( true)
            {
                inp_line = inp_br.readLine();
                String red_line = lemmatize_sentence(inp_line);
                ArrayList< ArrayList< String>> test_case = parse_tagged_line( red_line);
                //ArrayList< ArrayList< String>> test_case = parse_tagged_line( inp_line);
                ArrayList< String> data = test_case.get( 0);
                ArrayList< String> true_labels = test_case.get( 1);
                for( int i = 0; i < data.size(); i++)
                {
                    if( !lvocab.contains( data.get( i)))
                        data.set( i, "_UNK_");
                    if( !ltagset.contains( true_labels.get( i)))
                        true_labels.set( i, "_UNK_");
                }
                ArrayList< String> pred_labels, final_pred_labels;
                final_pred_labels = new ArrayList<String>();
//                pred_labels = tagger_1.best_tag_sequence( data);
//                for( int i = 0; i < data.size(); i++)
//                    System.out.print( data.get( i) + "_" + pred_labels.get( i) + " ");
//                System.out.println();
//                pred_labels = tagger_2.best_tag_sequence( data);
//                for( int i = 0; i < data.size(); i++)
//                    System.out.print( data.get( i) + "_" + pred_labels.get( i) + " ");
//                System.out.println();
//                pred_labels = tagger_3.best_tag_sequence( data);
//                for( int i = 0; i < data.size(); i++)
//                    System.out.print( data.get( i) + "_" + pred_labels.get( i) + " ");
//                System.out.println();
//                pred_labels = tagger_4.best_tag_sequence( data);
//                for( int i = 0; i < data.size(); i++)
//                    System.out.print( data.get( i) + "_" + pred_labels.get( i) + " ");
//                System.out.println();
                pred_labels = tagger_5.best_tag_sequence( data);
                for( int i = 0; i < unify_index.size(); i++)
                {
                    Integer ptr = unify_index.get( i);
                    String unified_word = "";
                    unified_word += (data.get( ptr).split("_"))[0];
                    unified_word += "_";
                    unified_word += pred_labels.get( ptr);
                    final_pred_labels.add( unified_word);
                    //unified_word += (true_labels.get( i).split(" "))[0];
                    //unified_word += (pred_labels.get( i).split("_"))[1];
                }
                for( int i = 0; i < final_pred_labels.size(); i++)
                    System.out.print( final_pred_labels.get( i) + " ");
                System.out.println();
//                for( int i = 0; i < data.size(); i++)
//                    System.out.print( data.get( i) + "_" + pred_labels.get( i) + " ");
                System.out.println();
                //pred_labels = tagger_6.best_tag_sequence( data);
                //for( int i = 0; i < data.size(); i++)
//                    System.out.print( data.get( i) + "_" + pred_labels.get( i) + " ");
//                System.out.println();
                
            }

        }
        //</editor-fold>

    }

    private InteractiveDriver()
    {
    }

    private InteractiveDriver(boolean b, boolean b0)
    {
        data_generated = b;
        model_generated = b0;
    }
    
    public boolean isModel_generated()
    {
        return model_generated;
    }
    
    public boolean isData_generated()
    {
        return data_generated;
    }
    
    public boolean isData_splitted()
    {
        return data_splitted;
    }
    
    public boolean isData_unk_fixed()
    {
        return data_unk_fixed;
    }
    
    public boolean isFreq_calculated()
    {
        return freq_calculated;
    }
    
    public boolean isGood_calculated()
    {
        return good_calculated;
    }    
    
    public boolean isTesting_done()
    {
        return testing_done;
    }
    
    public static ArrayList< ArrayList< String>> parse_tagged_line( String tagged_sentence)
    {
        ArrayList< ArrayList< String>> ret = new ArrayList< ArrayList< String>>();
        ArrayList< String> words, tags;
        words = new ArrayList<String>();
        tags = new ArrayList<String>();        
        String[] tokens = tagged_sentence.split(" ");
        for( int i = 0; i < tokens.length; i++)
        {
            String[] word_tag = tokens[ i].split("_");
            words.add( word_tag[ 0]);
            tags.add(word_tag[ 1]);
        }
        ret.add( words);
        ret.add( tags);
        return ret;
    }
    
    private static void indi_inc(HashMap<String, Integer> map, String key)
    {
        if( !map.containsKey(key))
            map.put( key, 0);
        Integer val = map.get( key);
        map.put( key, val + 1);
    }
    
    private static void pair_inc(HashMap<String, HashMap< String, Integer>> map, String key1, String key2)
    {
        if( !map.containsKey( key1))
            map.put( key1, new HashMap< String, Integer>());
        if( !map.get( key1).containsKey( key2))
            map.get(key1).put(key2, 0);
        Integer val = map.get( key1).get( key2);
        map.get( key1).put( key2, val + 1);
    }
    
    public HashMap< String, HashMap< String, Integer>> calculate_confusion_matrix( ArrayList< String> true_labels, ArrayList< String> pred_labels)
    {
        HashMap< String, HashMap< String, Integer>> retmap = new HashMap< String, HashMap< String, Integer>>();
        HashSet< String> local_tagset = new HashSet< String>();
        for( String tag: true_labels)
            local_tagset.add( tag);
        for( String tag: pred_labels)
            local_tagset.add( tag);
        for( String tag: local_tagset)
            retmap.put( tag, new HashMap< String, Integer>());
        //retmap.put( "UNK", new HashMap< String, Integer>());
        for( String rtag: local_tagset)
            for( String ctag: local_tagset)
                retmap.get( rtag).put( ctag, 0);
        //retmap.get( "UNK").put( "UNK", 0);
        for( int cptr = 0; cptr < true_labels.size(); cptr ++)
        {
            String rtag = true_labels.get( cptr);
            String ctag = pred_labels.get( cptr);
            Integer val = retmap.get( rtag).get( ctag);
            try
            {
                retmap.get( rtag).put( ctag, val + 1);
            }
            catch( Exception e)
            {
                //System.out.println( rtag + " " + ctag);
                throw e;
            }
        }
        return retmap;
    }
    
    public Double calculate_accuracy( ArrayList< String> true_labels, ArrayList< String> pred_labels)
    {
        Double ans = true_labels.size() * 1.0;
        for( int i = 0; i < true_labels.size(); i ++)
            if( true_labels.get( i).equals( pred_labels.get( i)) == false)
                ans --;
        ans /= true_labels.size();
        return ans;
    }
    
    public Double calculate_precision( HashMap< String, HashMap< String, Integer>> confusion_matrix)
    {
        Double total = 0.0;
        int cnt = 0;
        for( String ctag: confusion_matrix.keySet())
        {
            Double val = 0.0;
            for( String rtag: confusion_matrix.keySet())
                val += confusion_matrix.get( rtag).get( ctag);
            if( confusion_matrix.get( ctag).get( ctag) == 0 && val == 0)
                continue;
            total += confusion_matrix.get( ctag).get( ctag) / val;
            cnt ++;
        }
        total /= cnt;
        return total;
    }
    
    public Double calculate_recall( HashMap< String, HashMap< String, Integer>> confusion_matrix)
    {
        Double total = 0.0;
        int cnt = 0;
        for( String rtag: confusion_matrix.keySet())
        {
            Double val = 0.0;
            for( String ctag: confusion_matrix.keySet())
                val += confusion_matrix.get( rtag).get( ctag);
            if( confusion_matrix.get( rtag).get( rtag) == 0 && val == 0)
                continue;
            total += confusion_matrix.get( rtag).get( rtag) / val;
            cnt ++;
        }
        total /= cnt;
        return total;
    }
    
    private static String lemmatize_sentence(String cur_line) throws IOException
    {
        String ret_sentence = "";
        unify_index = new ArrayList< Integer>();
        int ind_cnt = 0;
        String[] tokens = cur_line.split( " ");
        for( String cur_token : tokens)
        {
            String[] pair = cur_token.split( "_");
            String word = pair[ 0];
            String tag = pair[ 1];
            if( word.equals( "SSS"))
            {
                ret_sentence += ( word + "_" + tag);
                ret_sentence += " ";
                unify_index.add( ind_cnt);
                ind_cnt ++;
                continue;
            }
            word = word.toLowerCase();
            String word_tag = word + "_"+tag.substring(0,1).toLowerCase();
            sock_out.println( word_tag);
            //sock_out.write( word);
            String morphed_word = sock_in.readLine();
            try
            {
                String[] lemma_pairs = new String[ 2];
                lemma_pairs = morphed_word.split("_");
                ret_sentence += (lemma_pairs[0] + "_" + tag + " " + lemma_pairs[1] + "_" + lemma_pairs[1].toUpperCase());
                unify_index.add( ind_cnt);
                ind_cnt += 2;
            }
            catch (Exception e)
            {
                ret_sentence += ( morphed_word.substring(0, morphed_word.length()-1) + "_" + tag);
                unify_index.add( ind_cnt);
                ind_cnt ++;
            }
            if (!morphed_word.equals("."))
            {
                ret_sentence += " ";
            }
        }
        int tp = unify_index.size();
        return ret_sentence;
    }
    
}


