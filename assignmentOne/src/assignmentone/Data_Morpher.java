/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package assignmentone;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 *
 * @author coed-forensic
 */
public class Data_Morpher
{
    private static String data_prefix = "data/";
    private static String orig_fname = "proc_brown.txt";
    private static Socket morpher_socket;
    private static PrintWriter sock_out;
    private static BufferedReader sock_in;
    private static String new_fname = "new_morph_brown.txt";
    //private static String new_fname = "lower_brown.txt";
    //private static String new_fname = "morph_brown.txt";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException
    {
        //<editor-fold desc="Connect to python morph analyzer">
        try
        {
            morpher_socket = new Socket( "127.0.0.1", 12345);
            sock_out = new PrintWriter( morpher_socket.getOutputStream(), true);
            sock_in = new BufferedReader( new InputStreamReader( morpher_socket.getInputStream()));
        }
        catch( Exception e)
        {
            System.out.println( "" + e.getMessage());
        }
        //</editor-fold>

        
        //<editor-fold desc="Process data file">
        BufferedReader orig_br = new BufferedReader( new FileReader( data_prefix + orig_fname));
        PrintWriter pout = new PrintWriter( data_prefix + new_fname);
        String cur_line = "";
        String morphed_line = "";
        int cnt = 0;
        while ( true) 
        {
            cur_line = orig_br.readLine();
            if( cur_line == null)
                break;
            morphed_line = lemmatize_sentence( cur_line);
            System.out.println( "" + cnt);
            pout.println( morphed_line);
            cnt ++;
            //System.out.println( morphed_line);
        }
        pout.close();
        //</editor-fold>
    }

     private static String lemmatize_sentence(String cur_line) throws IOException
    {
        String ret_sentence = "";
        String[] tokens = cur_line.split( " ");
        for( String cur_token : tokens)
        {
            String[] pair = cur_token.split( "_");
            String word = pair[ 0];
            String tag = pair[ 1];
            if( word.equals( "SSS"))
            {
                ret_sentence += ( word + "_" + tag);
                ret_sentence += " ";
                continue;
            }
            word = word.toLowerCase();
            String word_tag = word + "_"+tag.substring(0,1).toLowerCase();
            sock_out.println( word_tag);
            //sock_out.write( word);
            String morphed_word = sock_in.readLine();
            try
            {
                String[] lemma_pairs = new String[ 2];
                lemma_pairs = morphed_word.split("_");
                ret_sentence += (lemma_pairs[0] + "_" + tag + " " + lemma_pairs[1] + "_" + lemma_pairs[1].toUpperCase());
            }
            catch (Exception e)
            {
                ret_sentence += ( morphed_word.substring(0, morphed_word.length()-1) + "_" + tag);
            }
            if (!morphed_word.equals("."))
            {
                ret_sentence += " ";
            }
        }
            
        return ret_sentence;
    }
    
}
