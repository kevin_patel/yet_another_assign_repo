/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package assignmentone;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author coed-forensic
 */
public class TestSerializeArrayList
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        ArrayList< ArrayList< String>> test_db = new ArrayList< ArrayList< String>>();
        ArrayList< ArrayList< String>> read_db = new ArrayList< ArrayList< String>>();
        ArrayList< String> test_single = new ArrayList< String>();
        test_single.add( "Hello");
        test_single.add( "World");
        test_db.add( test_single);
        test_single = new ArrayList< String>();
        test_single.add( "World");
        test_single.add( "seems");
        test_single.add( "fine");
        test_db.add( test_single);
        
        // serializing
        try
        {
            FileOutputStream fos = new FileOutputStream( "arr_serial.bin");
            ObjectOutputStream oos = new ObjectOutputStream( fos);
            oos.writeObject( test_db);
            oos.close();
            fos.close();
        }
        catch( Exception e)
        {
            System.out.println( "Thenga");
        }
        
        // deserializing
        try
        {
            FileInputStream fis = new FileInputStream( "arr_serial.bin");
            ObjectInputStream ois = new ObjectInputStream( fis);
            read_db = ( ArrayList< ArrayList< String>>)ois.readObject();
            for( ArrayList< String> curlist : test_db)
            {
                for( String curstr: curlist)
                    System.out.print( curstr + " ");
                System.out.println();
            }
            for( ArrayList< String> curlist : read_db)
            {
                for( String curstr: curlist)
                    System.out.print( curstr + " ");
                System.out.println();
            }
            if( test_db.equals( read_db))
                System.out.println( "Majaa");
            else
                System.out.println( "Dhajaa");
        }
        catch( Exception e)
        {
            System.out.println( "Panga");
        }
    }
    
}
