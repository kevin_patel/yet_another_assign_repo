/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package assignmentone;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author coed-forensic
 */
public class Bigram_BackOff_Unigram_with_Interpolation
{
    String data_prefix = "data/fold_";
    String most_probable_tag;
    HashSet< String> vocab;
    HashSet< String> tagset;
    HashMap< String, Integer> just_tag;
    HashMap< String, Integer> just_word;
    HashMap< String, HashMap< String, Integer>> tag_given_tag;
    HashMap< String, HashMap< String, Integer>> word_given_tag;
    HashMap< String, HashMap< String, Integer>> tag_given_word;
    static int error_cnt = 0;
    public static boolean total_computed;
    public static int total;
    
    public Bigram_BackOff_Unigram_with_Interpolation( int fold_ptr) throws FileNotFoundException, IOException, ClassNotFoundException
    {
        vocab = new HashSet< String>();
        tagset = new HashSet< String>();
        just_tag = new HashMap< String, Integer>();
        just_word = new HashMap< String, Integer>();
        tag_given_tag = new HashMap< String, HashMap< String, Integer>>();
        word_given_tag = new HashMap< String, HashMap< String, Integer>>();
        tag_given_word = new HashMap< String, HashMap< String, Integer>>();
        total_computed = false;
        
        FileInputStream fis;
        ObjectInputStream ois;
        fis = new FileInputStream( data_prefix + fold_ptr + "_vocab");
        ois = new ObjectInputStream( fis);
        vocab = ( HashSet< String>)ois.readObject();
        ois.close();
        fis.close();
        fis = new FileInputStream( data_prefix + fold_ptr + "_tagset");
        ois = new ObjectInputStream( fis);
        tagset = ( HashSet< String>)ois.readObject();
        ois.close();
        fis.close();
        fis = new FileInputStream( data_prefix + fold_ptr + "_just_tags");
        ois = new ObjectInputStream( fis);
        just_tag = ( HashMap< String, Integer>)ois.readObject();
        ois.close();
        fis.close();
        fis = new FileInputStream( data_prefix + fold_ptr + "_tag_given_word");
        ois = new ObjectInputStream( fis);
        tag_given_word = ( HashMap< String, HashMap< String, Integer>>)ois.readObject();
        ois.close();
        fis.close();
        fis = new FileInputStream( data_prefix + fold_ptr + "_tag_given_tag");
        ois = new ObjectInputStream( fis);
        tag_given_tag = ( HashMap< String, HashMap< String, Integer>>)ois.readObject();
        ois.close();
        fis.close();
        fis = new FileInputStream( data_prefix + fold_ptr + "_word_given_tag");
        ois = new ObjectInputStream( fis);
        word_given_tag = ( HashMap< String, HashMap< String, Integer>>)ois.readObject();
        ois.close();
        fis.close();
        fis = new FileInputStream( data_prefix + fold_ptr + "_just_word");
        ois = new ObjectInputStream( fis);
        just_word = ( HashMap< String, Integer>)ois.readObject();
        ois.close();
        fis.close();
        most_probable_tag = most_prob_tag();
    }
    
    public String most_prob_tag()
    {
        String candidate = "";
        Integer max_score = Integer.MIN_VALUE, cur_score;
        for( String tag: just_tag.keySet())
        {
            cur_score = just_tag.get( tag);
            if( max_score < cur_score)
            {
                max_score = cur_score;
                candidate = tag;
            }
        }
        return candidate;
    }
    
    public String most_prob_tag_per_word( String word)
    {
        String candidate = "";
        Integer max_score = Integer.MIN_VALUE, cur_score;
        for( String tag: tag_given_word.get( word).keySet())
        {
            cur_score = tag_given_word.get( word).get( tag);
            if( max_score < cur_score)
            {
                max_score = cur_score;
                candidate = tag;
            }
        }
        return candidate;
    }
    
    private static Integer total( HashMap<String, Integer> map)
    {
        if( total_computed)
            return total;
        total = 0;
        for( String state: map.keySet())
            total += map.get( state);
        total_computed = true;
        return total;
    }
    
    public ArrayList< String> best_tag_sequence( ArrayList< String> sentence)
    {
        // Data Structures
        HashMap< String, HashMap< Integer, Double>> seq_score = new HashMap< String, HashMap< Integer, Double>>();
        HashMap< String, HashMap< Integer, String>> back_ptr = new HashMap< String, HashMap< Integer, String>>();

        // Initialization
        Double max_score, cur_score;
        String candidate;
        int T = sentence.size();
        int N = tagset.size();
        for( String state: tagset)
        {
            seq_score.put( state, new HashMap< Integer, Double>());
            back_ptr.put( state, new HashMap< Integer, String>());
            for( Integer i = 0; i < T; i ++)
            {
                //seq_score.get( state).put( i, Double.NEGATIVE_INFINITY);
                seq_score.get( state).put( i, 0.0);
                back_ptr.get( state).put( i, "");
            }
        }
        seq_score.get("SSS").put(0, 1.0);           // BAD CODE ALERT
        back_ptr.get("SSS").put(0, "");             // BAD CODE ALERT
        
        // Iteration
        for( int i = 1; i < T; i++)
        {
            String cur_word = sentence.get( i);
            for( String cur_state: tagset)
            {
                max_score = 0.0;
                //max_score = 
                candidate = "";
                
                //Double emit_score = get_emission_prob( cur_state, cur_word);
                //Double emit_score = get_add_emission_prob( cur_state, cur_word);
                Double emit_score = get_emission_prob( just_tag, word_given_tag, cur_state, cur_word);
                if( emit_score == 0.0)
                    continue;

                for( String prev_state: tagset)
                {
                    if( prev_state.equals("."))
                        continue;
                    Double prev_score = seq_score.get( prev_state).get( i - 1);
                    
                    //Double trans_score = get_smooth_trans_prob( prev_state, cur_state);
                    Double trans_score = get_trans_prob( just_tag, tag_given_tag, prev_state, cur_state);

                    cur_score = prev_score * trans_score * emit_score;
                    //cur_score = prev_score + Math.log( trans_score) + Math.log( emit_score);
                    if( max_score < cur_score)
                    {
                        max_score = cur_score;
                        candidate = prev_state;
                    }
                }
                if( candidate.equals( ""))
                {
                    if( vocab.contains( cur_word))
                        candidate = most_prob_tag_per_word( cur_word);
                    else
                        candidate = most_probable_tag;
                }
                seq_score.get( cur_state).put(i, max_score);
                back_ptr.get( cur_state).put(i, candidate);
            }
        }
        
        ArrayList< String> C = new ArrayList< String>();
        for( int i = 0; i < T; i++)
            C.add( "");
        //max_score = Double.NEGATIVE_INFINITY;
        max_score = 0.0;
        candidate = "";
        for( String state: tagset)
        {
            cur_score = seq_score.get( state).get( T - 1);
            if( max_score < cur_score)
            {
                max_score = cur_score;
                candidate = state;
            }
        }
        
        try
        {
            C.set( T - 1, candidate);
            for( int i = T - 2; i >= 0; i--)
                C.set( i, back_ptr.get( C.get(i + 1)).get(i + 1));                                    
        }
        catch( Exception e)
        {
            C = new ArrayList< String>();
            for( String word: sentence)
            {
                if( vocab.contains( word)) 
                    C.add( most_prob_tag_per_word(word));
                else
                    C.add( most_probable_tag);
            }
            error_cnt ++;
        }
        return C;
    }    
    
    private static void indi_inc(HashMap<String, Integer> map, String key)
    {
        if( !map.containsKey(key))
            map.put( key, 0);
        Integer val = map.get( key);
        map.put( key, val + 1);
    }
    
    private static Double get_trans_prob( HashMap<String, Integer> map_un, HashMap<String, HashMap< String, Integer>> map_bi, String key1, String key2)
    {
        Double arg1 = 0.0, arg2 = 0.0;
        if( !map_un.containsKey( key2))
        {
            arg1 = map_un.get( key2) * 1.0;
            arg1 /= ( total( map_un) * 1.0);
        }
        if( !map_bi.containsKey( key1))
            arg2 =  0.0;
        else if( !map_bi.get( key1).containsKey( key2))
            arg2 = 0.0;
        else
        {
            arg2 = map_bi.get( key1).get( key2) * 1.0;
            arg2 /= map_un.get( key1) * 1.0;
        }
        return 0.31 * arg1 + 0.69 * arg2;
    }
    
    private static Double get_emission_prob( HashMap<String, Integer> map_un, HashMap<String, HashMap< String, Integer>> map_emit, String key1, String key2)
    {
        if( !map_emit.containsKey( key1))
            return 0.0;
        if( !map_emit.get( key1).containsKey( key2))
            return 0.0;
        Double num = map_emit.get( key1).get( key2) * 1.0;
        Double den = map_un.get( key1) * 1.0;
        return num / den;
    }
    
//    public Double get_trans_prob( String prev, String cur)
//    {
//        
//    }
}
