/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package assignmentone;

import com.sun.corba.se.impl.io.IIOPInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author coed-forensic
 */
public class Baseline_Unigram
{
    public HashSet< String> vocab;
    public HashSet< String> tagset;
    public HashMap< String, Integer> just_tag;
    public HashMap< String, HashMap< String, Integer>> tag_given_word;
    public String data_prefix = "data/fold_";
    public String most_probable_tag;
    
    public Baseline_Unigram( int fold_ptr) throws FileNotFoundException, IOException, ClassNotFoundException
    {
        just_tag = new HashMap< String, Integer>();
        tag_given_word = new HashMap< String, HashMap< String, Integer>>();
        
        FileInputStream fis;
        ObjectInputStream ois;
        fis = new FileInputStream( data_prefix + fold_ptr + "_vocab");
        ois = new ObjectInputStream( fis);
        vocab = ( HashSet< String>)ois.readObject();
        ois.close();
        fis.close();
        fis = new FileInputStream( data_prefix + fold_ptr + "_tagset");
        ois = new ObjectInputStream( fis);
        tagset = ( HashSet< String>)ois.readObject();
        ois.close();
        fis.close();
        fis = new FileInputStream( data_prefix + fold_ptr + "_just_tags");
        ois = new ObjectInputStream( fis);
        just_tag = ( HashMap< String, Integer>)ois.readObject();
        ois.close();
        fis.close();
        fis = new FileInputStream( data_prefix + fold_ptr + "_tag_given_word");
        ois = new ObjectInputStream( fis);
        tag_given_word = ( HashMap< String, HashMap< String, Integer>>)ois.readObject();
        ois.close();
        fis.close();
        most_probable_tag = most_prob_tag();
    }
    
    public String most_prob_tag()
    {
        String candidate = "";
        Integer max_score = Integer.MIN_VALUE, cur_score;
        for( String tag: just_tag.keySet())
        {
            cur_score = just_tag.get( tag);
            if( max_score < cur_score)
            {
                max_score = cur_score;
                candidate = tag;
            }
        }
        return candidate;
    }
    
    public String most_prob_tag_per_word( String word)
    {
        String candidate = "";
        Integer max_score = Integer.MIN_VALUE, cur_score;
        for( String tag: tag_given_word.get( word).keySet())
        {
            cur_score = tag_given_word.get( word).get( tag);
            if( max_score < cur_score)
            {
                max_score = cur_score;
                candidate = tag;
            }
        }
        return candidate;
    }
    
    public ArrayList< String> best_tag_sequence( ArrayList< String> sentence)
    {
        ArrayList< String> ret = new ArrayList< String>();
        for( String word: sentence)
        {
            if( vocab.contains( word))
                ret.add( most_prob_tag_per_word( word));
            else
                ret.add( most_probable_tag);
        }
        return ret;
    }
}
